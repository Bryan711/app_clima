//
//  ViewController.swift
//  App_Clima
//
//  Created by BRYAN OCAÑA on 25/10/17.
//  Copyright © 2017 BRYAN OCAÑA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    //MARK:-ViewController LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    //MARK:- Actions

    @IBAction func loginButtonPressed(_ sender: Any) {
        
        let user = userTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        switch (user, password) {
        case ("bryan","bryan"):
            //ejecutamos segues y sabemos que vista podemos llamar
            performSegue(withIdentifier: "ciudadSegue", sender: self)
        case ("bryan", _):
            mostrarAlerta(mensaje: "Contraseña incorrecta")
        default:
            mostrarAlerta(mensaje: "usuario y contraseña incorrectos")
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func invitadoButtonPressed(_ sender: Any) {
    }
    
    //para que el logueo se quede en blanco si regresamos
    override func viewWillDisappear(_ animated: Bool) {
        userTextField.text = ""
        passwordTextField.text = ""
        //para que el foco se quede al inicio
        userTextField.becomeFirstResponder()
    }
    
    private func mostrarAlerta(mensaje:String){
        
        let alertaView = UIAlertController(title: "Error", message: mensaje,
                                           preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default){(action)in
            self.userTextField.text = ""
            self.passwordTextField.text = ""
        }
        alertaView.addAction(aceptar)
        present(alertaView, animated: true, completion: nil)
}
}
