//
//  InvitadoViewController.swift
//  App_Clima
//
//  Created by BRYAN OCAÑA on 25/10/17.
//  Copyright © 2017 BRYAN OCAÑA. All rights reserved.
//

import UIKit
import CoreLocation

//conformar un delegado:
class InvitadoViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var nameCityLabel: UILabel!
    
    let locationManager = CLLocationManager()
    
    //MARK:-
    //var bandera = false
    //MARK:- ViewController lifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //implementanos el delagado
        
        locationManager.requestWhenInUseAuthorization()
        
        //si da permiso
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self//aqui conformamos el delegado
            locationManager.startUpdatingLocation()//captura la ubicaicon del usuario
        }
        
    }
    
    //MARK:- LocatioManager Delegate
    //manager toda lainformacion la location latitud y longitud
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
       // if !bandera{
            consultaPorUbicacion(
                lat: (location?.latitude)!,
                lon: (location?.longitude)!
            )
        //}
        //bandera = true
         locationManager.stopUpdatingLocation()
    }
    
    
    //MARK:- Actions
    private func consultaPorUbicacion(lat:Double, lon:Double){
        let servicio = Servicio()
        servicio.consultaUbicacionActual(lat: lat, lon: lon) { (weather, ciudad) in
            DispatchQueue.main.async {
                self.weatherLabel.text = weather
                self.nameCityLabel.text = ciudad
            }
        }
    }
}
