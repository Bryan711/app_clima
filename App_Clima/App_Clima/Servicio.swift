//
//  Servicio.swift
//  App_Clima
//
//  Created by BRYAN OCAÑA on 7/11/17.
//  Copyright © 2017 BRYAN OCAÑA. All rights reserved.
//

import Foundation

class Servicio{
    func ConsultaPorCiudad(city:String, completion:@escaping (String, String)->()){
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=de779e79b5f13d49b9aa161a116d2aa1"
        
        consulta(urlStr: urlStr){ ( weather, ciudad) in
            completion(weather, ciudad)
            
        }
    }
    
    func consultaUbicacionActual(lat:Double, lon:Double, completion:@escaping (String, String)->()){
        let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=de779e79b5f13d49b9aa161a116d2aa1"
        consulta(urlStr: urlStr){ (weather,ciudad) in
            completion(weather,ciudad)
        }
    }
    //scaping sale del hilo
    //tipp de datos que vas a regresar
    func consulta(urlStr:String, completion:@escaping (String, String) -> ()){
        
        let url =  URL(string: urlStr)
        let request = URLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            print("Error: \(error)")
            //print("Response: \(response)")
            //print("Data: \(data)")
            
            if let _ = error {
                return
            }
    
            do{
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                
                let weatherDict = weatherJson as! NSDictionary
                
                //cuadbo este vacion el campo
                //todas las opciones que metamos en el guard debe ser opcional y cambiamos ! por ?
                //despues q sale del guard deja de ser opcional
                guard let weatherKey = weatherDict["weather"] as? NSArray else {
    
                    //para mandar el error y q me retorne si esta bien o no so esta mal
                    //DispatchQueue.main.async {
                       // self.weatherLabel.text = "Ciudad no valida"
                    //}
                    return
                }
                let weather = weatherKey[0] as! NSDictionary
                let ciudad = weatherDict["name"] as! String
                //setearle en el label
                //para q se sepa q estamos hablando del view controller
                
                //joins
                //DispatchQueue.main.async {
                   // self.weatherLabel.text = "\(weather["description"] ?? "Error")"
                //}
                completion("\(weather["description"] ?? "")", "\(ciudad)")
                //print (type(of: weatherLson))
                //print(weather)
                //print("--------")
                //print(type(of: weather))
                
            } catch {
                print("Error al generar el Json")
            }
        }
        //Ejecutamos
        task.resume()
        
        }
}
