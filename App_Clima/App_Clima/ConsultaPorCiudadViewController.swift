//
//  ConsultaPorCiudadViewController.swift
//  App_Clima
//
//  Created by BRYAN OCAÑA on 31/10/17.
//  Copyright © 2017 BRYAN OCAÑA. All rights reserved.
//

import UIKit

    //MARK:- ViewController lifeCicle

class ConsultaPorCiudadViewController: UIViewController {

    //MARK:-Outlets
    
    @IBOutlet weak var cityTextfield: UITextField!
    @IBOutlet weak var weatherLabel: UILabel!
    
    //MARK:- Actions
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func consultarButtonPressed(_ sender: Any) {
        let servicio = Servicio()
        servicio.ConsultaPorCiudad(city: cityTextfield.text!){ (weather, ciudad) in
            DispatchQueue.main.async {
                self.weatherLabel.text = weather
                self.cityTextfield.text = ciudad
            }
        }
    }

    
}
